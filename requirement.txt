h5py==2.8.0
Keras==2.2.4
tensorflow-gpu==1.13.1
dlib==19.16.0
opencv_python==3.4.0.12
imutils==0.5.1
numpy==1.15.2
matplotlib==3.0.0
scipy==1.1.0
tqdm==4.13.1
pandas==0.24.2