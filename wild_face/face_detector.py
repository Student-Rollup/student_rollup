import os
import tensorflow as tf

from wild_face.networks.yolov3.utils import *
from wild_face.networks.yolov3.yolo_v3 import Yolo_v3


_MODEL_SIZE = (416, 416)
_CLASS_NAMES_FILE = os.path.dirname(__file__) + '/networks/yolov3/data/labels/face.names'
_MAX_OUTPUT_SIZE = 100


class YoloFaceDetector(object):
    def __init__(self, model_path, iou_threshold, confidence_threshold):
        self.model_path = model_path
        self.class_names = load_class_names(_CLASS_NAMES_FILE)
        self.n_classes = len(self.class_names)
        self.model = Yolo_v3(n_classes=self.n_classes, model_size=_MODEL_SIZE,
                             max_output_size=_MAX_OUTPUT_SIZE,
                             iou_threshold=iou_threshold,
                             confidence_threshold=confidence_threshold)

    def detect_face(self, input_names):
        batch_size = len(input_names)
        batch = load_images(input_names, model_size=_MODEL_SIZE)
        inputs = tf.placeholder(tf.float32, [batch_size, *_MODEL_SIZE, 3])
        detections = self.model(inputs, training=False)
        saver = tf.train.Saver(tf.global_variables(scope='yolo_v3_model'))

        with tf.Session() as sess:
            saver.restore(sess, self.model_path)
            detection_result = sess.run(detections, feed_dict={inputs: batch})
        draw_boxes(input_names, detection_result, self.class_names, _MODEL_SIZE)
        img = cv2.imread(input_names[0])
        resize_factor = (img.shape[1] / _MODEL_SIZE[1], img.shape[0] / _MODEL_SIZE[0])
        boxes = []

        for box in detection_result[0][0]:
            xy, confidence = box[:4], box[4]
            xy = [int(xy[i] * resize_factor[i % 2]) for i in range(4)]
            boxes.append((xy, confidence))

        print('Detections have been saved successfully.')
        return boxes

    def detect_face_by_image(self, im):
        batch_size = 1
        batch = process_image(image=im, model_size=_MODEL_SIZE)
        inputs = tf.placeholder(tf.float32, [batch_size, *_MODEL_SIZE, 3])
        detections = self.model(inputs, training=False)
        saver = tf.train.Saver(tf.global_variables(scope='yolo_v3_model'))

        with tf.Session() as sess:
            saver.restore(sess, self.model_path)
            detection_result = sess.run(detections, feed_dict={inputs: batch})
        resize_factor = (im.shape[1] / _MODEL_SIZE[1], im.shape[0] / _MODEL_SIZE[0])
        boxes = []

        for box in detection_result[0][0]:
            xy, confidence = box[:4], box[4]
            xy = [int(xy[i] * resize_factor[i % 2]) for i in range(4)]
            boxes.append((xy, confidence))

        print('Detections have been saved successfully.')

        return boxes


if __name__ == '__main__':
    model = YoloFaceDetector(model_path=os.path.dirname(__file__) + '/weights/yolov3/model.ckpt',
                             iou_threshold=0.5,
                             confidence_threshold=0.5)
    # im = cv2.imread('./tests/images/mn2.jpg')
    x = model.detect_face(['./tests/images/nm2.jpg'])
    # x = model.face_detected_by_image(im)
    # for i in x:
    #     a = i[0]
    #     cv2.rectangle(im, (a[0], a[1]), (a[2], a[3]), (255, 0 ,100), 5)
    #     cv2.imshow("im", im)
    #     cv2.waitKey(0)
    #     cv2.destroyAllWindows()
