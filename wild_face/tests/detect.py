"""Yolo v3 detection script.

Saves the detections in the `detection` folder.

Usage:
    python detect.py <images/video> <iou threshold> <confidence threshold> <filenames>

Example:
    python detect.py images 0.5 0.5 data/images/dog.jpg data/images/office.jpg
    python detect.py video 0.5 0.5 data/video/shinjuku.mp4

Note that only one video can be processed at one run.
"""

import tensorflow as tf
import sys
import cv2
import  numpy as np

from wild_face.networks.yolov3.yolo_v3 import Yolo_v3
from wild_face.networks.yolov3.utils import load_images, load_class_names, draw_boxes, draw_frame

_MODEL_SIZE = (416, 416)
_CLASS_NAMES_FILE = './networks/yolov3/data/labels/face.names'
_MAX_OUTPUT_SIZE = 100
# model = FACENET_ONESHOT(path_weight='weights/nn4.small2.v1.h5',
#                             path_landmask='shape_predictor_68_face_landmarks.dat',
#                             path_train='training/*')

# train_embs, label2idx = model.train_or_load(batch_size=128, cons=False)

def main(type, iou_threshold, confidence_threshold, input_names):
    class_names = load_class_names(_CLASS_NAMES_FILE)
    n_classes = len(class_names)

    model = Yolo_v3(n_classes=n_classes, model_size=_MODEL_SIZE,
                    max_output_size=_MAX_OUTPUT_SIZE,
                    iou_threshold=iou_threshold,
                    confidence_threshold=confidence_threshold)

    if type == 'images':
        batch_size = len(input_names)
        batch = load_images(input_names, model_size=_MODEL_SIZE)
        inputs = tf.placeholder(tf.float32, [batch_size, *_MODEL_SIZE, 3])
        detections = model(inputs, training=False)
        saver = tf.train.Saver(tf.global_variables(scope='yolo_v3_model'))

        with tf.Session() as sess:
            saver.restore(sess, './yolov3/weights/model.ckpt')
            detection_result = sess.run(detections, feed_dict={inputs: batch})
            img = cv2.imread('/home/duongpd/FRA/test_image/test1.jpg')
            print(img.shape)
            resize_factor = (img.shape[1] / _MODEL_SIZE[1], img.shape[0] / _MODEL_SIZE[0])
            print(resize_factor)
            box = detection_result[0][0][0]
            print(box)
            xy, confidence = box[:4], box[4]
            xy = [int(xy[i] * resize_factor[i % 2]) for i in range(4)]
            print(xy)
            # print("xy:\n{}\nx0,y0:\n{},{}".format(xy,x0, y0))
            cv2.circle(img, (int(xy[0]),int(xy[1])), 10, (255, 255, 0), 1)
            cv2.circle(img, (int(xy[2]), int(xy[3])), 10, (0, 255, 0),1)
            cv2.imshow("im", cv2.resize(img, (1280, 1080)))
            cv2.waitKey(0)
            cv2.destroyAllWindows()
        draw_boxes(input_names, detection_result, class_names, _MODEL_SIZE)

        print('Detections have been saved successfully.')

    elif type == 'video':
        inputs = tf.placeholder(tf.float32, [1, *_MODEL_SIZE, 3])
        detections = model(inputs, training=False)
        saver = tf.train.Saver(tf.global_variables(scope='yolo_v3_model'))

        with tf.Session() as sess:
            saver.restore(sess, './yolov3/weights/model.ckpt')

            win_name = 'Video detection'
            cv2.namedWindow(win_name)
            cap = cv2.VideoCapture(0)
            frame_size = (cap.get(cv2.CAP_PROP_FRAME_WIDTH),
                          cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
            fourcc = cv2.VideoWriter_fourcc(*'X264')
            fps = cap.get(cv2.CAP_PROP_FPS)
            print(fps)
            out = cv2.VideoWriter('./yolov3/detections/detections.mp4', fourcc, fps,
                                  (int(frame_size[0]), int(frame_size[1])))

            try:
                while True:
                    ret, frame = cap.read()
                    if not ret:
                        break
                    resized_frame = cv2.resize(frame, dsize=_MODEL_SIZE[::-1],
                                               interpolation=cv2.INTER_NEAREST)
                    detection_result = sess.run(detections,
                                                feed_dict={inputs: [resized_frame]})

                    draw_frame(frame, frame_size, detection_result,
                               class_names, _MODEL_SIZE)

                    cv2.imshow(win_name, frame)

                    key = cv2.waitKey(1) & 0xFF

                    if key == ord('q'):
                        break

                    out.write(frame)
            finally:
                cv2.destroyAllWindows()
                cap.release()
                print('Detections have been saved successfully.')

    else:
        raise ValueError("Inappropriate data type. Please choose either 'video' or 'images'.")


if __name__ == '__main__':
    main(sys.argv[1], float(sys.argv[2]), float(sys.argv[3]), sys.argv[4:])
