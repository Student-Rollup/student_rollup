import datetime
import os
import threading
import time
from urllib.request import urlopen

import cv2
import numpy as np
import pyrebase

from wild_face.face_detector import YoloFaceDetector
from wild_face.face_recognitor import FaceNetOneShotRecognitor

# Config path
path = os.path.dirname(os.path.dirname(__file__))

# Init face_detection model
face_detection_model = YoloFaceDetector(model_path=path + "/weights/yolov3/model.ckpt",
                                        iou_threshold=0.5,
                                        confidence_threshold=0.5)

# Init face_recognition model
face_recognition_model = FaceNetOneShotRecognitor(path_weight=path+'/weights/facenet/nn4.small2.v1.h5',
                                                  path_landmask=path+'/weights/facenet/shape_predictor_68_face_landmarks.dat',
                                                  path_train=path+'/training/*')

train_embs, label2idx = face_recognition_model.train_or_load(batch_size=128,
                                                             cons=False)

# Config firebase
config = {
    "apiKey": "AIzaSyCeUCxnYqP59tosAA_bCddWRGWhJCiekSI",
    'authDomain': "student-rollup.firebaseapp.com",
    'databaseURL': 'https://student-rollup.firebaseio.com/',
    'storageBucket': 'student-rollup.appspot.com',
}

# Initialize firebase app
firebase = pyrebase.initialize_app(config=config)
db = firebase.database()
storage = firebase.storage()
font = cv2.FONT_HERSHEY_DUPLEX
WHITE = [255, 255, 255]


def active(img):
    face_locations = face_detection_model.detect_face_by_image(im=img)
    faces = []
    for face_location in face_locations:
        x1, y1, x2, y2 = face_location[0]
        face = img[y1:y2, x1:x2]
        faces.append(face)
    face_names, unknown = face_recognition_model.predict(faces=faces,
                                                         train_embs=train_embs,
                                                         label2idx=label2idx)
    print(face_names)
    print(len(unknown))
    for i in unknown:
        cv2.imshow("im", np.squeeze(i))
        cv2.waitKey(0)

    # Push data
    for name in face_names:
        today = datetime.datetime.today()
        day = datetime.datetime.strftime(today, "%d-%m-%Y")
        time = datetime.datetime.strftime(today, "%H:%M")
        db.child("DIEMDANHSINHVIEN_DIHOC").child('MACLENIN1_BIS2').child('Attendance').child(
            "{}".format(name)).child("{}".format(day)).set({
            "KETQUA": 1,
            "Url": "gs://student-rollup.appspot.com/{}/Blacklist/unknown.jpg".format(day),
            "THOIGIAN": time})


# METHOD #1: OpenCV, NumPy, and urllib
def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    resp = urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    # return the image
    return image


def check_msg(data):
    if data is None:
        return None
    else:
        x = data
        if type(x) is dict:
            if len(x) > 1:
                return x
            else:
                return x.popitem()[1]

        elif type(x) is str:
            return x


# Streaming handel firebase
def stream_handler(message):
    try:

        print(message["event"])  # put
        print(message["path"])  # /-K7yGTTEp7O549EzTYtI

        result = check_msg(message["data"])
        print(result)
        if type(result) is str:
            url = storage.child(result).get_url(token=False)
            print(url)
            img = url_to_image(url)
            active(img)
            # data = db.child("Get").child(1).get().val()
            # print(data)
    except Exception as e:
        print("{} went wrong".format(e))


def run():
    d = datetime.datetime.today()
    today = datetime.datetime.strftime(d, "%d-%m-%Y")
    db.child("DIEMDANHSINHVIEN_DIHOC").child("MACLENIN1_BIS2").child("Image").child(
        "{}".format(today)).stream(stream_handler)
    time.sleep(0.01)


if __name__ == '__main__':
    t = threading.Thread(target=run)
    t.start()
    t.join()
